package demo;

import javax.persistence.*;

@Entity
@Table(name="weapons")
public class Weapons {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Integer id;

	String name;
	Integer damage;
	Integer itemlevel;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getItemlevel() {
		return itemlevel;
	}
	public void setItemlevel(Integer itemlevel) {
		this.itemlevel = itemlevel;
	}
	public Integer getDamage() {
		return damage;
	}
	public void setDamage(Integer dmg) {
		damage=dmg;
	}
	
	public Weapons(){
		
	}
	
	public Weapons(String name, Integer damage, Integer itemlevel){
		this.name=name;
		this.damage=damage;
		this.itemlevel=itemlevel;
	}
}
