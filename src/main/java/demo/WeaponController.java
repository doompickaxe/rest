package demo;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/weapon")
public class WeaponController {

	@Autowired
	WeaponsRepository weap;
	
    @RequestMapping(value="/",method=RequestMethod.GET)
    public Collection<Weapons> getAllWeapons() {
        return (Collection<Weapons>)weap.findAll();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Weapons getOneWeapon(@PathVariable Integer id){
    	return weap.findOne(id);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Weapons> create(@RequestParam(value="name", required=true)String name, 
    		@RequestParam(value="damage", required=true)Integer damage, 
    		@RequestParam(value="itemlevel", required=true)Integer itemlevel) {
    	
    	Weapons w=new Weapons(name, damage, itemlevel);
    	weap.save(w);
        return new ResponseEntity<Weapons>(w,HttpStatus.OK);
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void delete(@PathVariable Integer id)
	{
		weap.delete(id);
	}
    
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public Weapons update(@PathVariable Integer id, @RequestParam(value="name", required=true) String name,
			@RequestParam(value="damage", required=true) Integer damage,
			@RequestParam(value="itemlevel", required=true) Integer itemlevel)
	{
		Weapons w = weap.findOne(id);
		w.setName(name);
		w.setDamage(damage);
		w.setId(itemlevel);
		return w;
	}
}
