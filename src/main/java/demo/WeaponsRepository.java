package demo;

import org.springframework.data.repository.CrudRepository;

import demo.Weapons;

public interface WeaponsRepository extends CrudRepository<Weapons, Integer>{
}
